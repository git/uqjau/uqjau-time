#!/usr/bin/env bash

# --------------------------------------------------------------------
# Synopsis: Given a GNU future-date-string; will sleep 'til then.
#   Acceptable args are those honored by 'date --date'.
#   Ex
#     $ourname now +20 min
#     $ourname 10am Monday
# --------------------------------------------------------------------
# Notes: may be sourced for it's function "sleep_until"
# --------------------------------------------------------------------
# Rating: tone: tool used: monthly stable: y notable: y
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2004, 2010, 2011 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

# -- Software License --
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---

sleep_until() {
  (
  set -e
  local future=$(date --date "$*")
  local delta_seconds=$(($(date --date "$future" "+%s") -  $(date '+%s')))

  if [[ $delta_seconds -gt 0 ]];then
    echo "sleeping until: [$future]" >&2
    sleep $delta_seconds
  else
    echo \[$future] not clearly in future >&2
    return 1
  fi
  )
}

# main()
{
   return 0 2>/dev/null || true ### Don't run main() if script was sourced.
                                ###  Returning from a sourced script *is* legal
                                ###  in bash 2.03.0(2)-release.
                                ###   
                                ###  Return fails with a syntax error msg when
                                ###  calling (not sourcing), in which case we 
                                ###  want to continue on in main. The "|| true"
                                ###  is meant to underline the intention of continuing
                                ###  on when the return fails.
                                ###   
                                ###  FYI: "return"ing from a sourced file causes
                                ###  core dumps or hangs under linux bash
                                ###  version 1.14.7(1).
                                 
  sleep_until "$*" 
}
