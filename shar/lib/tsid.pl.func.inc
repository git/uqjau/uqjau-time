#!/bin/false
# (should be sourced by perl)

# --------------------------------------------------------------------
# Synopsis: tsid function: represent current time in seconds as
# lower case 7 char base 36 string
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2009, 2014 Tom Rodman
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2011/10/21 00:32:14 $   (GMT)
# $Revision: 1.4 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/time/shar/lib/RCS/tsid.pl.func.inc,v $
#      $Log: tsid.pl.func.inc,v $
#      Revision 1.4  2011/10/21 00:32:14  rodmant
#      *** empty log message ***
#
#      Revision 1.3  2009/10/21 13:03:50  rodmant
#      *** empty log message ***
#
#      Revision 1.2  2009/08/08 22:10:06  rodmant
#      *** empty log message ***
#
#      Revision 1.1  2009/08/04 12:30:11  rodmant
#      Initial revision
#
#
# --------------------------------------------------------------------

# -------------------------------------------------------------------- 
# Subroutines 'digitize' and 'to_base' taken from
# http://www.rosettacode.org/wiki/Number_base_conversion
# in April 2009.  ( GNU FDL 1.3 )
# -------------------------------------------------------------------- 
sub digitize
# Converts an integer to a single digit.
{ my $i = shift;
  return $i < 10
    ? $i
    : ('a' .. 'z')[$i - 10];
}

sub to_base
{ my ($int, $radix) = @_;
  my $numeral = digitize($int % $radix);
  $numeral .= digitize($int % $radix)
      while $int = int($int / $radix);
  return scalar reverse $numeral;
}

sub tsid
{
  # Usage: ourname UNIX_EPOC_SEC

  # ourname will convert UNIX_EPOC_SEC to seconds since 12/14/1901, 
  # ie since -2147450400 (in UNIX epoc scale):
    # $ date --date '12/14/1901' +%s
    # -2147450400

  my $epoch_offset = -2147450400;
    # $ date --date '12/13/1901' +%s
    # date: invalid date `12/13/1901'
    # $ date --date '12/14/1901' +%s
    # -2147450400

  my $mytime;

  if (@_)
  { $mytime = shift @_; }
  else
  { $mytime = time(); }

  $out = "000000"  .  to_base($mytime - $epoch_offset, 36);

    # Will break when $out above is more than 7 chars.
    # Below we determine this is over 2474 years:

      # $ type -a Bc
      # Bc is a function
      # Bc () 
      # { 
      #     : _func_ok2unset_ team function;
      #     : --------------------------------------------------------------------;
      #     : Synopsis: Wrapper for 'bc'. Defines an exponential function;
      #     : 'p (a,b) { return (e  ( l (a) * b )) }';
      #     : --------------------------------------------------------------------;
      #     { 
      #         echo 'define p (a,b) {
      #             return (e  ( l (a) * b )) 
      #           }';
      #         echo scale=3;
      #         cat
      #     } | bc -lq
      # }
      # $ echo 'p(36,7)/3600/24/365.25'|Bc
      # 2474.206

  return substr($out,-7)
}

1;
