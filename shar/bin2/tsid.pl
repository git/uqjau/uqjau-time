#!/usr/bin/env perl
$^W=1;
  # equivalent to 'perl -w'
use strict;

# --------------------------------------------------------------------
# Synopsis: calls tsid function which converts current time in
# seconds as lower case 6 char base 36 string
# --------------------------------------------------------------------
# Usage: $ourname
# --------------------------------------------------------------------
# Rating: tone: script-support tool used: yes stable: y TBDs: n interesting: y noteworthy: y
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) Aug 2009 Tom Rodman <Rodman.T.S@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2011/10/21 00:36:34 $   (GMT)
# $Revision: 1.7 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/time/shar/bin2/RCS/tsid.pl,v $
#      $Log: tsid.pl,v $
#      Revision 1.7  2011/10/21 00:36:34  rodmant
#      *** empty log message ***
#
#      Revision 1.6  2009/10/21 13:01:19  rodmant
#      *** empty log message ***
#
#      Revision 1.5  2009/10/21 13:00:58  rodmant
#      *** empty log message ***
#
#      Revision 1.4  2009/08/04 12:29:38  rodmant
#      *** empty log message ***
#
#      Revision 1.1  2009/07/18 23:55:48  rodmant
#      Initial revision
#
# --------------------------------------------------------------------

our($_29r, $_29rev, $_29lib,$ourname);

use Cwd 'realpath';
BEGIN{
# --------------------------------------------------------------------
# define directory definitions for our world, in several env vars
# --------------------------------------------------------------------
  my ($startdir, $dirname);
  $startdir= realpath;

  $0 =~ m{^(.*)/} and $dirname=$1;
  chdir "$dirname/../.." || die;
    #cd to parent dir of parent dir of parent dir of $0
    #we die if $0 has no "/"; that's OK

  $_29r= realpath;

  chdir "$startdir" || die;

  ($_29rev = $dirname ) =~ s~^.*/~~;
    # basename of $dirname

  require "$_29r/package/$_29rev/main/etc/dirdefs.perl";
    #not security risk if $0 is in correct commands dir
}

## main
require "$_29lib/tsid.pl.func.inc";

print &tsid . "\n";
