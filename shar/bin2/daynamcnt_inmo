#!/usr/bin/env bash
set -e
set -u

# --------------------------------------------------------------------
# Synopsis: echo count of dayname instances for current date
# --------------------------------------------------------------------
# Usage: $ourname
# ex
#
#   $ date; command cal ;$_c/daynamcnt_inmo
#   Sat Oct 22 11:49:20 CDT 2011
#       October 2011
#   Su Mo Tu We Th Fr Sa
#                      1
#    2  3  4  5  6  7  8
#    9 10 11 12 13 14 15
#   16 17 18 19 20 21 22
#   23 24 25 26 27 28 29
#   30 31
#   Sat:4
#
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2009 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

# == Software License ==
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>.
# ---

# --------------------------------------------------------------------
# $Source: /usr/local/7Rq/package/cur/time/shar/bin2/RCS/daynamcnt_inmo._m4,v $
# $Date: 2011/10/22 16:51:59 $ GMT    $Revision: 1.8 $
# --------------------------------------------------------------------

# *****************************************************************
# define env var dir-defs for our "uqjau tools"
# *****************************************************************
cd ${0%/*}/../../
# $(dirname $0)
  # cd 2 dirs up from ( ${0%/*} == $(dirname $0) )
  # if  $0 has no "/"; 'source' below fails/we abort, that's OK (we do not support . in path)
_29r=$PWD
cd "$OLDPWD"

cd ${0%/*}
   # dirname of $0
_29rev=${PWD##*/}
   # Basename of dir below 'commands', 'scommands' or 'lib'; it contains the soft link
   # to this script.
cd "$OLDPWD"

if ! test -f $_29r/package/$_29rev/main/etc/dirdefs; then
  # create default dirdefs 
  source $_29r/package/$_29rev/main/slib/seed_dirdefs.shinc
else
  source $_29r/package/$_29rev/main/etc/dirdefs
    # not security risk if $0 is in correct commands dir
    # dirdefs appends $_29rev suffix to some defs
fi

help()
{
cat <<__HELP_EOF

-${ourname}-
Synopsis: Your synopsis here ...

Usage: $ourname your syntax here ...

Example:
 $ourname your example here ...
 results: your example results here ...

Example:
 $ourname
 results:

FILE
  external file dependencies listed here
ENV
  external env var dependencies listed here

__HELP_EOF

exit 0
}

source $_29lib/bash_common.shinc
  # for summary of what is provided run this:
  #   sed -n '2,/-end-of-help/p' $_lib/bash_common.shinc|less

case ${1:-} in
  --hHeElLpP) help "$@" ;exit ;;
esac

# **************************************************
# Main procedure.
# **************************************************
#main
{

_email_on_unexpected_exit=${_email_on_unexpected_exit:-yes}
_email_if_dying_on_trapped_sig=${_email_if_dying_on_trapped_sig:-no}
# _email_if_errlog_exists=yes # for Example "errlog foo" => e-mail

# ==================================================
# Process optional parameters.
# ==================================================
opt_true=1 OPTIND=1
  # OPTIND=1 only needed for 2nd and subsequent getopt invocations
  # since shell's start up sets it to 1
while getopts : opt_char
  do
     # Check for errors.
     case $opt_char in
       \?) >&2 echo "Unexpected option(-$OPTARG)."
           exit 1;
           ;;
        :) >&2 echo "Missing parameter for option(-$OPTARG)."
           exit 1;
           ;;
        p) >&2 echo "Option '-p' is currently not supported."
           exit 1
           ;;
     esac

     # Record the info in an "OPT_*" env var.
     eval OPT_${opt_char}="\"\${OPTARG:-$opt_true}\""
  done

shift $(( $OPTIND -1 ))
unset opt_true opt_char

# --------------------------------------------------------------------{
#   read rcfile ( for env var defs );
#   local rcfile for local setting, else look for site setting
#
#   to see what dirs $_29eloc and $_29team are for:
#     awk '/## shared script rc files/, /^#-/' $_m/etc/dirdefs
#
# --------------------------------------------------------------------
for dir in $_29eloc $_29team
do
  rcfile=$dir/${ourname}rc
    # env vars impacted by rcfile:
    #   imp!! LIST_VARS_CHANGED_HERE
  if test -s "$rcfile"
  then
    source "$rcfile"
    break #prefer local file
  fi
done
unset rcfile dir
# }

# **************************************************
# script start:
# **************************************************

possible_last_one_flag=""
day_of_mo=$(date +%d)
day_of_wk=$(date +%a)

last_day_of_mo=$(
  date --date "$(date --date 'next month' '+%m/1/%Y') -1 day" +%d
)

weeks=$[$day_of_mo/7]
remainder=$[$day_of_mo % 7 ]

# show_var day_of_mo last_day_of_mo weeks remainder

count=$weeks
if test "$remainder" = 0 
then :
else
 count=$[$count +1]
fi 

if test $[$last_day_of_mo - $day_of_mo] -lt 7
then
  possible_last_one_flag=:last
fi

echo $day_of_wk:$count$possible_last_one_flag

# ==================================================
# main done.
# ==================================================
: dropping through to main end #set -x makes this echo
exit 0

} #main end
